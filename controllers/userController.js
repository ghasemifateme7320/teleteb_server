const express = require('express');
const app = express();
const bodyParser = require('body-parser');
app.use(bodyParser.json());
const userAdapter = require("./../database/userAdapter");
const uuidv1 = require('uuid/v1');
let accessTokenMap = {};


app.post("/users", (req, res)=>{
	console.log("request body",req.body);
	userAdapter.createUser(req.body, function(error, result){
		if (error) {
			res.send(error)
		}else{
			res.json({"message":"success"})
			console.log("user post correctly" )
		}
		console.log("result:",result)
	});

});

app.get("/login", (req,res)=>{
	let username = req.query.username;
	let password = req.query.password;
	userAdapter.searchUserByUsername(username, (error, user)=>{
		if (error){
			res.send(error);
		}
		else {
			let uuid = uuidv1();
			accessTokenMap[uuid] = username;
			console.log("req.query.pqssword:",req.query);
			console.log('user  : ', user);
			if (user.password != null) {
				if (user.password == req.query.password){
					res.json({"username":username,"accessToken":uuid})}
				else {
					res.json({"error":"password is wrong"})
				}
			}
			else {
				res.json({"error": "there is no such a user"})
			}

		}
	})
});
 function getUserDataFromAccessToken(accessToken) {
 	return accessTokenMap[accessToken];

 }

function listen(port) {
	var server = app.listen(port, function () {
		var host = server.address().address;
		var port = server.address().port;
		console.log("Example app listening at http://%s:%s", host, port)
	});

}


module.exports = {
	getUserDataFromAccessToken,
	listen

}