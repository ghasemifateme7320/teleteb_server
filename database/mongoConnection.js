const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/teleTeb');
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'db connection error...'));

let userSchema = new mongoose.Schema({
	"username"  : String,
	"password"  :String
});

module.exports = {
	user: mongoose.model('user', userSchema, 'user')
}