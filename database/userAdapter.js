const userSchema = require('./mongoConnection').user;

function createUser(userData, callback){
	userSchema.findOneAndUpdate(
		{"username":userData.username
		},
		userData,
		{upsert:true}

	).exec(callback)
}

function searchUserByUsername(username,callback){
	userSchema.findOne( {  username  }).exec(callback)
}


module.exports ={
	createUser,
	searchUserByUsername

};
