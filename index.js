const getUserDataFromAccessToken = require('./controllers/userController').getUserDataFromAccessToken;
const listen = require('./controllers/userController').listen;
const server = require('http').createServer();
const io = require('socket.io')(server);
let loggedIn_client =[];
const joinChat = " joined chat";
const loginSuccess= "loginSuccess";
const newMessage = "newMessage";
const connection = "connection";
const login='login';
const loginFail="loginFail";
const sendMessage="sendMessage";
const disconnect = "disconnect";
const leftChat = " left chat";



listen(6500);

function sendMessageToAllClient(message) {
    loggedIn_client.forEach(item => {
        item.emit(newMessage, message)
    })
}


function isRegistered(data) {
    return getUserDataFromAccessToken(data.accessToken)
}


function loginUsers(data,client) {
	console.log("logUsers");
    if (isRegistered(data)) {
        console.log("data:",data);
        client.emit(loginSuccess, data);
        client.name = getUserDataFromAccessToken(data.accessToken);
        loggedIn_client.push(client);
        //setTimeout because server send join message before client activity can show it
        setTimeout(() => {
        	console.log("client:",client.name);
            sendMessageToAllClient(client.name + joinChat);
        }, 2000)
    }
    else {
        client.emit(loginFail)
    }
}

io.on(connection, client => {
    client.on(login, (data) => {
        //data is user information
        loginUsers(data,client);
    });

    client.on(sendMessage, data => {
        sendMessageToAllClient(client.name + " : " + data)
    });


    client.on(disconnect, () => {
        sendMessageToAllClient(client.name + leftChat)
    });

});


server.listen(6300, () => {
    console.log("Well done, now I am listening on ", server.address().port)
});

module.exports = {
    isRegistered,
    loginUsers,
    sendMessageToAllClient
}

